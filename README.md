# AngularPokemon

An online trainer page where pokemon can be added and removed from profile using a pokemon catalogue.

## Description

The application features 3 main components:

* A Startup page - where you can "login on" to the application with a trainer name of choice.

* A pokemon catalogue - where all pokemon can be seen and added to a profile.

* A trainer page - where you can see and remove pokemon you have caught.

## Installation

Dependencies: https://github.com/dewald-els/noroff-assignment-api

Recommended runtime: [Node.js - LTS version](https://nodejs.org)

Install the project
```
1. git clone https://gitlab.com/mfrykman/angular-pokemon.git
2. npm install
```
Create environment variables
```

## Usage
```
1. ng serve
2. Browse to http://localhost:4200
```

3. Go inside the 2 environments files (placed inside src folder) and add the key and url for the noroff-assigment-api:

USER_API_Key: ...
USER_API_URL: ...
```

## Contributors

* [NKE @nicolai_eng](@nicolai_eng)
* [MF @mfrykman](@mfrykman)

## License
[MIT](https://choosealicense.com/licenses/mit/)
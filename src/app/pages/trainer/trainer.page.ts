import { Component, OnInit } from '@angular/core';
import { Poke } from 'src/app/models/poke.model';
import { User } from 'src/app/models/user.model';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';


// Pokemon image api
const pokeApiPng = environment.POKE_API_PNG;

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})

export class TrainerPage implements OnInit {
  // Initialize lists
  public favList: string[] = [];
  public yourPoke: Poke[] = [];
  public userId: string = "";

  // Initalize service
  constructor(private readonly catService: CatalogueService,
    private readonly userService: UserService) { }

  // Update favourite pokemons initially or when button is pressed
  updateFavorites(remPoke?: string): void {
    this.catService.getFavourite(this.userId).subscribe(
      (favList: User) => {
        this.favList = favList.pokemon; 

        // if input => pokemon has been removed from => update api
        if (remPoke) {
          this.favList = this.favList.filter(n => n !== remPoke)

          this.catService.setFavourite(this.favList,this.userId).subscribe()
        }
        // display favourite pokemon by updating pokelist for html
        this.dispFavPoke()
      }
    )
  }

  // Update pokelist for html
  dispFavPoke(): void {
    this.yourPoke = []
    this.favList.forEach(name => {
        let id = sessionStorage.getItem(name);
        this.yourPoke.push({id: `${id}`, name : name, url: pokeApiPng + `${id}.png`})
    });
  }

  // Trigger favourite updater when initializing
  ngOnInit(): void{
    if (this.userService.user){
      this.userId = `${this.userService.user.id}`;
    }

    this.updateFavorites();
  }

  // Trigger favourite updater pokemon is removed
  public onRemoveClick(name?: string) {
    this.updateFavorites(name)
  }
}
import { Component, OnInit } from '@angular/core';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})

export class LoginPage {

  // Get router and services
  constructor(
    private readonly router: Router,
    private readonly catService: CatalogueService
  ) { }

  // Save all pokemon to session storage
  ngOnInit(): void {
    this.saveData()
  }

  // Loop thorugh all 905 pokemon
  saveData() {
    for (let i = 1; i < 906; i++) {
      this.dispPokemon(i)
    }
  }

  // Fetch one pokemon at a time
  dispPokemon(id: number){
    this.catService.getPokemon(`${id}`).pipe(
      // insert some smart stuff
      ).subscribe(
      (pokemon: any) => {
        sessionStorage.setItem(`${id}`, pokemon.name);
        sessionStorage.setItem(pokemon.name, `${id}`);
      }
    )
  }

  // Navigate to catalogue when logged in.
  handleLogin(): void {
    this.router.navigateByUrl("/catalogue");
  }

}

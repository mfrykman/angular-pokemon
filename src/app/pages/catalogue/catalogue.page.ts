import { Component, OnInit } from '@angular/core';
import { Poke } from 'src/app/models/poke.model';
import { User } from 'src/app/models/user.model';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

// Pokemon image api
const pokeApiPng = environment.POKE_API_PNG;

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})

export class CataloguePage implements OnInit {
  // Initialize  lists
  public pokeList: Poke[] = []
  public favList: string[] = [];
  // Numbers pagination
  public indexNewStart: number = 1;
  public indexAdd: number = 12;
  public userId: string = "";
  // Pokeball picture api for html
  public pokeballApiPng = environment.POKEBALL_API_PNG;

  // Import service
  constructor(private readonly catService : CatalogueService,
    private readonly userService: UserService) { }
  
  // Display pokemons and insert favourite symbol
  ngOnInit(): void {
    if (this.userService.user){
      this.userId = `${this.userService.user.id}`;
    }
    
    this.dispPokeIndex(this.indexNewStart)
    this.favUpdate()
  }

  // Create list of pokemon being visualized
  dispPokeIndex(indexNewStart: number) {
    for (let i = indexNewStart; i < indexNewStart + this.indexAdd; i++) {
      let name = sessionStorage.getItem(`${i}`)

      if (name) {
        this.pokeList.push({id: `${i}`, name : name, url: pokeApiPng + `${i}.png`})
      }
    }
  }
  
  // Update/find favourite pokemon
  public favUpdate(name?: string) {
    this.catService.getFavourite(this.userId).subscribe(
      (favList: User) => {
        this.favList = favList.pokemon;
        
        // If name input => changes in favourties
        if (name) {
          let pokemonHTML = document.getElementById(`button_${name}`);

          // Check if pokemon has been added or removed.
          if (pokemonHTML && pokemonHTML.innerHTML == "Remove from inventory") {
            this.favList = this.favList.filter(n => n !== name)
            this.remFavItem(name)
            }
          else {
            this.favList = [...this.favList, name]
            this.addFavItem(name)
          }

          // Add changes to favourites in API  
          this.catService.setFavourite(this.favList,this.userId).subscribe()
        }
        
        else {
          // Run through all favourites on initialization
          this.favList.forEach(el => {
            this.addFavItem(el)
          });
        }
      }
    )
  }

  // Add pokaball and correct button to favourite items 
  public addFavItem(name: string) {
    let pokemonHTML = document.getElementById(`button_${name}`);
    let pokeballHTML = document.getElementById(`pokeBall_${name}`)

    if (pokemonHTML && pokeballHTML) {
        pokeballHTML.style.display = "block";
        pokemonHTML.innerHTML = "Remove from inventory"
        pokemonHTML.style.backgroundColor = "rgb(152, 63, 63)"
    }
  }

  // Remove pokaball and add correct button to now non-favourite items
  public remFavItem(name: string) {
    let pokemonHTML = document.getElementById(`button_${name}`);
    let pokeballHTML = document.getElementById(`pokeBall_${name}`)

    if (pokemonHTML && pokeballHTML) {
        pokeballHTML.style.display = "none";
        pokemonHTML.innerHTML = "Add to inventory"
        pokemonHTML.style.backgroundColor = "rgb(225, 110, 110)"
    }
  }

  // Expand pokedex when button is clicked.
  public onExpandClick() {
    this.dispPokeIndex(this.indexNewStart += this.indexAdd)
    this.favUpdate()
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { USER_API_URL, USER_API_Key} = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public login(username:string): Observable<Trainer> {
    return this.checkUsername(username)
    .pipe(
      switchMap((user: Trainer | undefined) => {
        if (user === undefined) {
          return this.createTrainer(username);
        }
        return of(user);
      })
    )
}


//Check if user exists
private checkUsername(username: string): Observable<Trainer | undefined> {
  return this.http.get<Trainer[]>(`${USER_API_URL}?username=${username}`)
  .pipe(
    map((response: Trainer[]) => response.pop())
  )
}

// Create user

private createTrainer(username: string): Observable<Trainer> {
const trainer = {
  username,
  pokemon: []
};

const headers = new HttpHeaders({
  "Content-Type": "application/json",
  "x-api-key": USER_API_Key
})
return this.http.post<Trainer>(USER_API_URL, trainer, {
  headers
})
}

}

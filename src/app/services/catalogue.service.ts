import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const pokeApiUrl = environment.POKE_API_URL;
const userApiUrl = environment.USER_API_URL;
const userApiKey = environment.USER_API_Key;

const headers = new HttpHeaders({
  "content-type": "application/json",
  "x-api-key": userApiKey
})

@Injectable({
  providedIn: 'root'
})

export class CatalogueService {
  
  // Import http functionality.
  constructor(
    private readonly http: HttpClient
    ) {}


  // Fetch pokemon from api
  getPokemon(id: string): Observable<any> {
    return this.http.get<any>(`${pokeApiUrl}/${id}`)
  }

  // Fetch favourite pokemon from user api.
  getFavourite(userId: string): Observable<User>{
    return this.http.get<User>(`${userApiUrl}/${userId}`,
    {
      headers
    })
  }

  // Set favourite pokemon in user api
  setFavourite(pokeList: string[], userId: string): Observable<string[]> {
    return this.http.patch<string[]>(`${userApiUrl}/${userId}`, {
      pokemon: [...pokeList]
    },
    {
      headers
    })
  }
}

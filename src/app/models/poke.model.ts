export interface Poke {
    id?:string;
    name?: string;
    url?: string;
}
export const environment = {
  production: true,
  POKE_API_URL: "https://pokeapi.co/api/v2/pokemon",
  POKE_API_PNG: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/",
  POKEBALL_API_PNG: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/poke-ball.png",
};
